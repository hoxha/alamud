from .event import Event1

class KneelEvent(Event1):
    NAME = "kneel"

    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        self.inform("kneel")

    def context(self):
        context = super().context()
        context['object'] = self.actor
        return context
