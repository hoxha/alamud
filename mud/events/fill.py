from .event import Event3

#Remplir un récipient avec du liquide.
class FillWithEvent(Event3):
    NAME = "fill-with"

    def perform(self):
        if (not (self.object.has_prop("fillable") and self.object2.has_prop("liquid"))):
            self.fail()
            return self.inform("fill-with.failed")
        self.inform("fill-with")
