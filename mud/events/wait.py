from .event import Event1

class WaitEvent(Event1):
    NAME = "wait"

    #Ce qui manquait pour résoudre le bug des événements non recherchés dans le lieu.
    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        self.inform("wait")   #Dans event.py

    #Le seul truc que je ne comprends toujours pas est quand cette méthode est elle appelée? Je suppose dans evented.py, le self.context() dans world_context()
    def context(self):    #On garde le contexte normal, auquel on ajoute une entrée pour "object", non présente par défaut.
        context = super().context()
        context['object'] = self.actor  #En fait, il n'y a aucun objet associé, mais les fonctions appelées par l'utilisation d'un ChangePropEffect exigent l'utilisation d'une clé 'object'.
        return context
        #L'actor n'est pas concerné pas le changement de propriété généralement, mais ce n'est pas grave, car Propertied.change_prop
        #se débrouillera pour que la propriété soit changée pour l'objet réellement concerné.
