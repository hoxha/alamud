# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TakeEvent(Event2):
    NAME = "take"

    def perform(self):
        if not self.object.has_prop("takable"):
            self.add_prop("object-not-takable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object.move_to(self.actor)
        if (self.actor.container().id == "salle-rochers-000" and self.object.id == "medaillon-000"):  #Hack dégueulasse pour changer la propriété du lieu quand on y dépose le médaillon.
            self.actor.container().remove_prop("has_medaillon")
        self.inform("take")

    def take_failed(self):
        self.fail()
        self.inform("take.failed")

"""    def context(self):
        context = super().context()
        context['location'] = self.actor.container()
        return context
"""
