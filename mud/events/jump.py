from .event import Event1

class JumpEvent(Event1):
    NAME = "jump"

    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        self.inform("jump")

    def context(self):
        context = super().context()
        context['object'] = self.actor
        return context
